package com.infosys.learning.service;

import com.infosys.learning.dto.Person;
import com.infosys.learning.model.User;
import com.infosys.learning.dto.UserRequest;
import com.infosys.learning.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

@Service
public class LearningService {
    @Autowired
    UserRepository userRepository;

    public String getName(String gender) {
        Person person = new Person();
        if ("man".equals(gender)) {
            person.setName("John");
        } else {
            person.setName("Jane");
        }
        return person.getName();
    }

    public Person getNameV2(String gender) {
        Person person = new Person();
        if ("man".equals(gender)) {
            person.setName("John");
        } else {
            person.setName("Jane");
        }
        return person;
    }

    public String getNameV3(String name) {
        String fullName = "not identified";

        if ("John".equals(name)) {
            fullName = "John Doe";
        } else if ("Jane".equals(name)) {
            fullName = "Jane Doe";
        }

        return fullName;
    }


    public String getPerson(Person person) {

        person.setData();
        return person.toString();


    }


    public String register(UserRequest userRequest){

        User exitsUser= userRepository.findByUserName(userRequest.getUsername());
        if (exitsUser!=null){
            return "Register Failed, User Already Exits";
        }
        User user=new User();
        user.setUserName(userRequest.getUsername());
        user.setPassWord(userRequest.getPassword());
        userRepository.save(user);

        return user.toString();
    }

    public String login(UserRequest userRequest) {
        User exitsUser= userRepository.findByUserNameAndPassWord(userRequest.getUsername(),userRequest.getPassword());
        if (exitsUser!=null){
            return exitsUser.toString();
        }
        return "  ";

    }
}
