package com.infosys.learning.dto;

import java.time.Year;

public class Person {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Person() {
    }

    String name;
    int yearOfBirth;
    Data data= new Data();



    public int getyearOfBirth() {
        return yearOfBirth;
    }

    public int getAge(){
        int age = Year.now().getValue() - getyearOfBirth();
        return age;
    }

    public void setData(){
        this.data.setAge(getAge());
    }

   public Data getData(){
        return data;
   }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", data=" + data +
                '}';
    }
}
