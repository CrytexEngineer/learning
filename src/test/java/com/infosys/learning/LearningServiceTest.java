package com.infosys.learning;

import com.infosys.learning.dto.Person;
import com.infosys.learning.dto.UserRequest;
import com.infosys.learning.repository.UserRepository;
import com.infosys.learning.service.LearningService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class LearningServiceTest {


    @Autowired
    private LearningService learningService;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void getName() {
        String name = learningService.getName("man");
        Assert.assertEquals("John", name);
    }

    @Test
    public void getNameV3() {
        String name = learningService.getNameV3("John");
        Assert.assertEquals("John Doe", name);
    }


    @Test
    public void getPerson() {
        String name = learningService.getNameV3("man");
        Assert.assertEquals(name, "not identified");
    }

    @Test
    public void register() {
        String response = learningService.register(new UserRequest("Alan", "Muhammad Aqil"));
        Assert.assertEquals("Register Failed, User Already Exits", response);

    }

    @Test
    public void login() {
        String response = learningService.login(new UserRequest("Alan", "Muhammad Aqil"));
        Assert.assertEquals(userRepository.findByUserName("Alan").toString(), response);
    }

    @Test
    public void getNameV2() {
        Person response = learningService.getNameV2("man");
        Assert.assertEquals("John",response.getName());
    }


}

